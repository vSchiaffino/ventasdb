﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2Ventas.DTO
{
    class VentaCabecera
    {
        public int Id;
        public string Fecha;
        public int IdCliente;
        public int IdVendedor;
        public string Observaciones;
    }
}
