﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2Ventas.DTO
{
    public class Cliente
    {
        public int Id;
        public string Nombre;
        public string Direccion;
        public string Telefono;
        public string Email;
    }
}
