﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2Ventas.DTO
{
    class VentaDetalle
    {
        public int Id;
        public int IdVentaCabecera;
        public int IdArticulo;
        public int PrecioUnitario;
        public int Cantidad;
    }
}
