﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2Ventas.DTO
{
    public class Vendedor
    {
        public int Id;
        public string Nombre;
        public string Apellido;
        public string Legajo;
        public string Turno;
    }
}
