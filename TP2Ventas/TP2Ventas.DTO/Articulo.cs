﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2Ventas.DTO
{
    class Articulo
    {
        public int Id;
        public string Nombre;
        public string Descripcion;
        public int PrecioVenta;
        public int PrecioCompra;
        public int Stock;
    }
}
